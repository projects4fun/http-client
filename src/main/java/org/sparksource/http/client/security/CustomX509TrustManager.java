package org.sparksource.http.client.security;

import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;
import java.security.*;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * <p>A {@link X509TrustManager} which does additional validation that the
 * JVM internal implementation does not perform.</p>
 *
 * @author Juan Garcia
 * @since 2018-04-03
 */
public class CustomX509TrustManager implements X509TrustManager {

    final private Logger logger = Logger.getLogger(this.getClass().getName());

    final private X509Certificate[] trustedCerts;
    final private X509TrustManager x509TrustManager;

    private boolean isValidateCaChain;

    /**
     * <p>Bootstraps {@link X509TrustManager} with a java {@link KeyStore} and
     * {@link TrustManagerFactory}.</p>
     *
     * @param keyStore            the {@link KeyStore} to use when extracting
     *                            {@link Certificate}s for clients
     * @param trustManagerFactory a {@link TrustManagerFactory} that has been
     *                            initialized with the {@link KeyStore}
     */
    public CustomX509TrustManager(final KeyStore keyStore, final TrustManagerFactory trustManagerFactory) {
        trustedCerts = getTrustedCerts(keyStore);
        x509TrustManager = (X509TrustManager) trustManagerFactory.getTrustManagers()[0];
    }

    @Override
    public void checkClientTrusted(final X509Certificate[] x509Certificates, final String authType)
            throws CertificateException {
        x509TrustManager.checkClientTrusted(x509Certificates, authType);
        logger.finest("Certificates were trusted. Verifying CA chain.");
        validateChain(x509Certificates);
    }

    @Override
    public void checkServerTrusted(final X509Certificate[] x509Certificates, final String authType)
            throws CertificateException {
        x509TrustManager.checkServerTrusted(x509Certificates, authType);
        logger.finest("Certificates were trusted. Verifying CA chain.");
        validateChain(x509Certificates);
    }

    /**
     * <p>Called right after the "client hello" phase of a SSLContextAccess
     * connection. This method uses the built at class instantiation array of
     * {@link X509Certificate} to instruct the client which {@link Certificate}s
     * are valid for this connection.</p>
     *
     * @return an array of {@link X509Certificate}s trusted certs
     */
    @Override
    public X509Certificate[] getAcceptedIssuers() {
        return Arrays.copyOf(trustedCerts, trustedCerts.length);
    }

    /**
     * <p>Determines which {@link Certificate}s will be presented to a client
     * when a client attempts to perform a secure connection with the server.
     * Servers started define a Java {@link KeyStore} that may hold many
     * {@link Certificate} entries. In regards to the Java {@link KeyStore}
     * private key that may be a chain - only the leaf entity certificate is
     * added to the {@link X509Certificate} array that is ultimately presented
     * to the client.</p>
     *
     * <p>This method only runs at class initialization.</p>
     *
     * @param keyStore the {@link KeyStore} with all known {@link Certificate}s
     * @return an array of {@link X509Certificate}s trusted certs
     */
    private X509Certificate[] getTrustedCerts(final KeyStore keyStore) {
        List<X509Certificate> x509CertificateList = new ArrayList<>();

        try {
            Enumeration aliases = keyStore.aliases();

            while (aliases.hasMoreElements()) {
                final String alias = (String) aliases.nextElement();
                logger.finer(() -> String.format("Adding certificate alias of %s to the list.", alias));

                if (keyStore.isCertificateEntry(alias)) {
                    Certificate certificate = keyStore.getCertificate(alias);
                    if (certificate instanceof X509Certificate) {
                        x509CertificateList.add((X509Certificate) certificate);
                    }
                } else if (keyStore.isKeyEntry(alias)) {
                    // do not include complete CA chain. if the CA chain should
                    // be presented the CA chain should be added to the keystore.
                    Certificate[] certificates = keyStore.getCertificateChain(alias);

                    if (certificates != null && certificates.length > 0 && certificates[0] instanceof X509Certificate) {
                        x509CertificateList.add((X509Certificate) certificates[0]);
                    }
                }
            }
        } catch (KeyStoreException e) {
            logger.log(Level.SEVERE, "", e);
        }

        logger.finer(() -> String.format("found %s certificates to present to the client.", x509CertificateList.size()));
        return x509CertificateList.toArray(new X509Certificate[]{});
    }

    /**
     * <p>Attempts to validate a certificate chain. A CA chain is validated in a
     * sliding window fashion at any point there should be two certificates in
     * scope. The first certificate is a certificate that should have been
     * signed by the second. The final certificate is always self signed this
     * item is intentionally skipped.</p>
     *
     * <p>Servers that allow certificate chains to be included as PEM files allow
     * a client certificate to be associated with an incorrect CA.</p>
     *
     * @param x509Certificates the CA chain that was presented this this application.
     * @throws CertificateException if the CA chain is not valid. A CA chain is
     *                              invalid when the parent CA did not sign the
     *                              child certificate.
     */
    public void validateChain(final X509Certificate[] x509Certificates) throws CertificateException {
        for (int i = 0; i < x509Certificates.length; i++) {
            final X509Certificate currentCertificate = x509Certificates[i];

            // the root certificate of any CA chain is always self signed. skip parent key validation on this one.
            if ((i + 1) == x509Certificates.length) {
                break;
            }

            final X509Certificate nextCertificate = x509Certificates[i + 1];

            final PublicKey publicKey = nextCertificate.getPublicKey();
            try {
                currentCertificate.verify(publicKey);
            } catch (NoSuchAlgorithmException | InvalidKeyException | SignatureException | NoSuchProviderException e) {
                logger.log(Level.INFO, e, () -> String.format("Invalid CA chain. Certificate %s was not signed by certificate %s.",
                        currentCertificate.getSubjectDN().getName(), nextCertificate.getSubjectDN().getName()));

                if (isValidateCaChain) {
                    throw new CertificateException(e);
                }
            }
        }
    }

    public void validateChain(final List<X509Certificate> x509Certificates) throws CertificateException {
        validateChain(x509Certificates.toArray(new X509Certificate[]{}));
    }

    public boolean isValidateCaChain() {
        return isValidateCaChain;
    }

    public void setValidateCaChain(boolean validateCaChain) {
        isValidateCaChain = validateCaChain;
    }
}
