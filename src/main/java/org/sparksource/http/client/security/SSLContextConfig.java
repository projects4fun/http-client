package org.sparksource.http.client.security;

/**
 * <p>Defines properties about how the a HTTP request should behave when
 * interacting with SSL resources.</p>
 *
 * @author Juan Garcia
 * @since 2018-04-03
 */
public interface SSLContextConfig {

    KeystoreItem keystoreItem();

    default SSLContextAlgorithm sslContextAlgorithm() {
        return SSLContextAlgorithm.TLSv1_2;
    }

    default boolean isValidateCaChain() {
        return true;
    }
}
