package org.sparksource.http.client.security;

/**
 * <p>Interface used to define properties of the keystore to be used when
 * connecting to SSL resources.</p>
 *
 * @author Juan Garcia
 * @since 2018-04-03
 */
public interface KeystoreItem {

    /**
     * <p>The file system location where a key store can be found</p>
     *
     * @return the path to the keystore relative or absolute
     */
    String ksPath();

    /**
     * <p>The password to be able to open the key store</p>
     *
     * @return the password to access the keystore
     */
    String ksPass();
}
