package org.sparksource.http.client.security;

/**
 * <p>Configuration on the optional supported TLS versions.</p>
 *
 * @author Juan Garcia
 * @since 2018-04-03
 */
public enum SSLContextAlgorithm {

    TLS("TLS"),
    TLSv1("TLSv1"),
    TLSv1_1("TLSv1.1"),
    TLSv1_2("TLSv1.2");

    private String algorithm;

    SSLContextAlgorithm(final String s) {
        this.algorithm = s;
    }

    public String toString() {
        return algorithm;
    }
}
