package org.sparksource.http.client.security;

import javax.net.ssl.*;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.*;
import java.security.cert.CertificateException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Juan Garcia
 * @since 2018-04-03
 */
public class SSLContextAccess {

    final private Logger logger = Logger.getLogger(this.getClass().getName());
    final private SSLContextConfig sslContextConfig;

    public SSLContextAccess(final SSLContextConfig sslContextConfig) {
        this.sslContextConfig = sslContextConfig;
    }

    /**
     * <p>Creates {@link SSLServerSocketFactory} from {@link #generateSSLContext()}.</p>
     *
     * @return a {@link SSLServerSocketFactory} from {@link SSLContext} that
     * {@link SSLContextConfig} helped create
     * @throws GeneralSecurityException a catch all {@link KeyStore} related exception
     */
    public SSLServerSocketFactory getSSLServerSocketFactory() throws GeneralSecurityException {
        return generateSSLContext().getServerSocketFactory();
    }

    /**
     * <p>Creates {@link SSLSocketFactory} from {@link #generateSSLContext()}.</p>
     *
     * @return {@link SSLSocketFactory} from the {@link SSLContext} that a
     * {@link SSLContextConfig} helped define
     * @throws GeneralSecurityException a catch all {@link KeyStore} related exception
     */
    public SSLSocketFactory getSSLSocketFactory() throws GeneralSecurityException {
        return generateSSLContext().getSocketFactory();
    }

    /**
     * <p>Creates a {@link SSLContext} from a key store file and password. The
     * created {@link SSLContext} uses a custom {@link X509TrustManager} that
     * performs additional checks on the certificates that the JVM does not
     * perform.</p>
     *
     * @return a {@link SSLContext} with configuration help from {@link SSLContextConfig}
     * @throws GeneralSecurityException a catch all {@link KeyStore} related exception
     */
    public SSLContext generateSSLContext() throws GeneralSecurityException {
        KeystoreItem keystoreItem = sslContextConfig.keystoreItem();

        try {
            KeyManagerFactory kmf = KeyManagerFactory.
                    getInstance(KeyManagerFactory.getDefaultAlgorithm());
            KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());

            try (FileInputStream fileInputStream = new FileInputStream(keystoreItem.ksPath())) {
                keyStore.load(fileInputStream, keystoreItem.ksPass().toCharArray());
            }
            kmf.init(keyStore, keystoreItem.ksPass().toCharArray());

            TrustManagerFactory tmf = TrustManagerFactory
                    .getInstance(TrustManagerFactory.getDefaultAlgorithm());
            tmf.init((KeyStore) null); // load jvm trust store
            tmf.init(keyStore); // load configure keystore certificates

            SSLContext sslContext = getSslContextInstance();

            // use custom trust manager. default does not do CA chain verification
            CustomX509TrustManager x509TrustManager =
                    new CustomX509TrustManager(keyStore, tmf);
            x509TrustManager.setValidateCaChain(sslContextConfig.isValidateCaChain());

            sslContext.init(kmf.getKeyManagers(),
                    new TrustManager[]{x509TrustManager}, null);

            return sslContext;
        } catch (CertificateException | UnrecoverableKeyException |
                KeyStoreException | NoSuchAlgorithmException | IOException |
                KeyManagementException e) {

            logger.log(Level.SEVERE, "", e);
            throw new GeneralSecurityException(e);
        }
    }

    /**
     * <p>Help define the version of TLS to be used. Defaults TLS v1.2</p>
     *
     * @return the version of TLS to be used
     * @throws NoSuchAlgorithmException if the algorithm cannot be found
     */
    private SSLContext getSslContextInstance() throws NoSuchAlgorithmException {
        if (sslContextConfig.sslContextAlgorithm() != null) {
            logger.finer(() -> String.format("Applying user defined SSLContextAlgorithm: %s",
                    sslContextConfig.sslContextAlgorithm().toString()));
            return SSLContext.getInstance(sslContextConfig.sslContextAlgorithm().toString());
        } else {
            return SSLContext.getInstance(SSLContextAlgorithm.TLSv1_2.toString());
        }
    }
}
