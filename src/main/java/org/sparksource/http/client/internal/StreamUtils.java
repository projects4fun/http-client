package org.sparksource.http.client.internal;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

/**
 * @author Juan Garcia
 * @since 2018-04-03
 */
public class StreamUtils {

    /**
     * <p>Converts a {@link InputStream} to a {@link String} using
     * {@link StandardCharsets#UTF_8}</p>
     *
     * @param inputStream the stream to be converted
     * @return the {@link String} representation of the stream
     * @throws IOException if reading from the stream fails
     */
    public static String toString(final InputStream inputStream)
            throws IOException {

        return toString(inputStream, StandardCharsets.UTF_8);
    }

    /**
     * <p>Converts a {@link InputStream} to a {@link String}.</p>
     *
     * @param inputStream the stream to be converted
     * @param charset     the {@link Charset} to be used for conversion
     * @return the {@link String} representation of the stream
     * @throws IOException if reading from the stream fails
     */
    public static String toString(final InputStream inputStream,
                                  final Charset charset)
            throws IOException {

        ByteArrayOutputStream result = new ByteArrayOutputStream();
        byte[] buffer = new byte[2048];
        int length;
        while ((length = inputStream.read(buffer)) != -1) {
            result.write(buffer, 0, length);
        }
        return result.toString(charset.name());
    }

    /**
     * <p>Transforms a {@link String} to a {@link InputStream}</p>
     *
     * @param s the {@link String} to be converted
     * @return the {@link InputStream} representation of the {@link String}
     */
    public static InputStream toInputStream(final String s) {
        return new ByteArrayInputStream(s.getBytes(StandardCharsets.UTF_8));
    }

    /**
     * <p>Provides a means of taking that data from an {@link InputStream} and
     * streaming it to an {@link OutputStream} by passing the {@link String}
     * buffers.</p>
     *
     * @param outputStream {@link OutputStream} that content should be placed at
     * @param inputStream  {@link InputStream} that content should be placed at
     * @throws IOException if reading from the {@link InputStream} fails;
     *                     if writing to {@link OutputStream} fails
     */
    public static void inputToOutput(final OutputStream outputStream,
                                     final InputStream inputStream)
            throws IOException {

        byte[] buffer = new byte[2048];
        int length;
        while ((length = inputStream.read(buffer)) != -1) {
            outputStream.write(buffer, 0, length);
        }
    }
}
