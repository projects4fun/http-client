package org.sparksource.http.client.internal;

import java.util.logging.Logger;

/**
 * Encoding / Decoding
 *
 * @author Juan Garcia
 * @since 2018-04-03
 */
public class Codec {

    final private static Logger logger = Logger.getLogger(Codec.class.getName());

    public static class Encode {

        /**
         * <p>Performs percent encoding based on RFC 3986 section 2.</p>
         *
         * @param c the character sequence to encode
         * @return a percent encoded {@link String}
         */
        public static String uriEncode(final CharSequence c) {
            if (c == null || c.length() == 0) {
                return "";
            }

            logger.finer(() -> String.format("Encoding: %s", c));
            StringBuilder stringBuilder = new StringBuilder(c.length());

            for (int i = 0; i < c.length(); i++) {
                Character character = c.charAt(i);
                switch (character) {
                    case ' ':
                        stringBuilder.append("%20");
                        break;
                    case ':':
                        stringBuilder.append("%3A");
                        break;
                    case '/':
                        stringBuilder.append("%2F");
                        break;
                    case '?':
                        stringBuilder.append("%3F");
                        break;
                    case '#':
                        stringBuilder.append("%23");
                        break;
                    case '[':
                        stringBuilder.append("%5B");
                        break;
                    case ']':
                        stringBuilder.append("%5D");
                        break;
                    case '@':
                        stringBuilder.append("%40");
                        break;
                    case '!':
                        stringBuilder.append("%21");
                        break;
                    case '$':
                        stringBuilder.append("%24");
                        break;
                    case '&':
                        stringBuilder.append("%26");
                        break;
                    case '^':
                        stringBuilder.append("%5E");
                        break;
                    case '\'':
                        stringBuilder.append("%27");
                        break;
                    case '(':
                        stringBuilder.append("%28");
                        break;
                    case ')':
                        stringBuilder.append("%29");
                        break;
                    case '*':
                        stringBuilder.append("%2A");
                        break;
                    case '+':
                        stringBuilder.append("%2B");
                        break;
                    case ',':
                        stringBuilder.append("%2C");
                        break;
                    case ';':
                        stringBuilder.append("%3B");
                        break;
                    case '=':
                        stringBuilder.append("%3D");
                        break;
                    default:
                        stringBuilder.append(character);
                }
            }

            return stringBuilder.toString();
        }
    }
}
