package org.sparksource.http.client;

import org.sparksource.http.client.internal.Codec;
import org.sparksource.http.client.internal.StreamUtils;
import org.sparksource.http.client.security.SSLContextAccess;
import org.sparksource.http.client.security.SSLContextConfig;

import javax.net.ssl.HttpsURLConnection;
import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.Proxy;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.GeneralSecurityException;
import java.util.Base64;
import java.util.List;
import java.util.Map;
import java.util.StringJoiner;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

/**
 * <p>A wrapper object around the Java {@link URLConnection} providing a fluent
 * chain compatible API.</p>
 *
 * @author Juan Garcia
 * @since 2018-04-03
 */
public class HttpClient implements AutoCloseable {

    final private static Logger logger = Logger.getLogger(HttpClient.class.getName());
    final private static Base64.Encoder encoder = Base64.getEncoder();

    final private URL url;
    final private Method method;

    private Charset charset = StandardCharsets.UTF_8;

    private static volatile ConnectionFactory connectionFactory = ConnectionFactory.DEFAULT;

    // do not call on "this" urlConnection. Instead rely on the getter for this as it
    // ensures that if it is not a new item is created.
    private URLConnection urlConnection;

    public enum Method {GET, POST, HEAD, OPTIONS, PUT, DELETE, TRACE}

    /**
     * <p>Declares the {@link URL} and {@link Method} of a {@link HttpClient}.
     * Keeping these constructors private ensures that the internal final variables
     * are defined.</p>
     *
     * @param url    the {@link String} representation of the {@link URL}
     * @param method the {@link Method} type POST, GET, HEAD, etc.
     * @throws HttpClientException if the provided {@link URL} is not valid
     */
    private HttpClient(final CharSequence url, final Method method) {
        try {
            this.url = new URL(url.toString());
            this.method = method;
        } catch (IOException e) {
            throw new HttpClientException(e);
        }

        logger.finer(() -> String.format("Request -> %s Method: %s", url, method));
    }

    /**
     * <p>Declares the {@link URL} and {@link Method} of a {@link HttpClient}.
     * Keeping these constructors private ensures that the internal final variables
     * are defined.</p>
     *
     * @param url    the remote system {@link URL}
     * @param method the {@link Method} type POST, GET, HEAD, etc.
     */
    private HttpClient(final URL url, final Method method) {
        this.url = url;
        this.method = method;

        logger.finer(() -> String.format("Request -> %s Method: %s",
                url.toString(), method));
    }

    /**
     * <p>Prepares a {@link HttpClient} item with a POST method.</p>
     *
     * @param url the {@link CharSequence} representation of the {@link URL}
     * @return this {@link HttpClient}
     */
    public static HttpClient post(final CharSequence url) {
        return new HttpClient(url, Method.POST);
    }

    /**
     * <p>Prepares a {@link HttpClient} item with a POST method.</p>
     *
     * @param url the remote system {@link URL}
     * @return this {@link HttpClient}
     */
    public static HttpClient post(final URL url) {
        return new HttpClient(url, Method.POST);
    }

    /**
     * <p>Prepares a {@link HttpClient} item with a GET method.</p>
     *
     * @param url the {@link CharSequence} representation of the {@link URL}
     * @return this {@link HttpClient}
     */
    public static HttpClient get(final CharSequence url) {
        return new HttpClient(url, Method.GET);
    }

    /**
     * <p>Prepares a {@link HttpClient} item with a GET method.</p>
     *
     * @param url the remote system {@link URL}
     * @return this {@link HttpClient}
     */
    public static HttpClient get(final URL url) {
        return new HttpClient(url, Method.GET);
    }

    /**
     * <p>Prepares a {@link HttpClient} item with a GET method and all declared
     * parameters.</p>
     *
     * @param url        the {@link CharSequence} representation of the {@link URL}
     * @param parameters a {@link List} of all URL parameters that to be added
     * @return this {@link HttpClient}
     */
    public static HttpClient get(final CharSequence url, final Map<String, String> parameters) {
        if (parameters == null) {
            return new HttpClient(url, Method.GET);
        }

        return new HttpClient(url + "?" + concatParameters(parameters), Method.GET);
    }

    /**
     * <p>Prepares a {@link HttpClient} item with a GET method and all declared
     * parameters.</p>
     *
     * @param url        the {@link CharSequence} representation of the {@link URL}
     * @param parameters a {@link List} of all URL parameters that to be added
     * @return this {@link HttpClient}
     */
    public static HttpClient get(final URL url, final Map<String, String> parameters) {
        if (parameters == null) {
            return new HttpClient(url, Method.GET);
        }

        return new HttpClient(url.toString() + "?" + concatParameters(parameters), Method.GET);
    }

    /**
     * <p>Internal HTTP parameter concatenation for the {@link #get(URL, Map)}
     * and {@link #get(CharSequence, Map)} methods.</p>
     *
     * @param parameters the {@link List} of URL parameters to be appended to
     *                   final URL
     * @return a {@link String} of all the parameters ready to use
     */
    private static String concatParameters(final Map<String, String> parameters) {
        StringJoiner stringJoiner = new StringJoiner("&");
        for (Map.Entry<String, String> httpParameter : parameters.entrySet()) {
            final String parameter = httpParameter.getKey() + "=" + Codec.Encode.uriEncode(httpParameter.getValue());
            logger.finer(() -> String.format("Setting a HTTP Parameter of: %s", parameter));
            stringJoiner.add(parameter);
        }
        return stringJoiner.toString();
    }

    /**
     * <p>Prepares a {@link HttpClient} item with a DELETE method..</p>
     *
     * @param url the {@link CharSequence} that a HTTP connection will be
     *            attempted to
     * @return this {@link HttpClient}
     */
    public static HttpClient delete(final CharSequence url) {
        return new HttpClient(url, Method.DELETE);
    }

    /**
     * <p>Prepares a {@link HttpClient} item with a DELETE method.</p>
     *
     * @param url the remote system {@link URL}
     * @return this {@link HttpClient}
     */
    public static HttpClient delete(final URL url) {
        return new HttpClient(url, Method.DELETE);
    }

    /**
     * <p>Performs a connection for a {@link URLConnection}. This method is not
     * expecting to send data to the remote server and provides a
     * {@link HttpClient} of the data that was identified.</p>
     *
     * @return this {@link HttpClient}
     * @throws IOException if the connection cannot be opened
     */
    public HttpClient send() throws IOException {
        logger.finer(() -> String.format("Performing an initial connect on %s.",
                url.toString()));
        getUrlConnection().connect();
        return this;
    }

    /**
     * <p>Performs a connection for a {@link URLConnection}. This method accepts
     * a {@link CharSequence} and transforms them to bytes and sends it to the
     * remote server.</p>
     *
     * @param charSequence the data that should be sent to the remote set a
     *                     {@link CharSequence}
     * @return this {@link HttpClient}
     * @throws IOException if we fail to write data to the {@link OutputStream}
     */
    public HttpClient send(final CharSequence charSequence) throws IOException {
        logger.finer(() -> String.format("Performing an initial connect on %s with defined output.",
                url.toString()));
        getUrlConnection().setDoOutput(true);

        logger.finer(() -> String.format("Sending the following data: %s",
                charSequence.toString()));
        try (OutputStream stream = getUrlConnection().getOutputStream()) {
            stream.write(charSequence.toString().getBytes(charset));
            stream.flush();
        }

        return this;
    }

    /**
     * <p>Performs a connection for a {@link URLConnection}. This method accepts
     * bytes as the content to be sent to the remote server.</p>
     *
     * @param bytes set of data as bytes to be sent to the remote site
     * @return this {@link HttpClient}
     * @throws IOException if we fail to write data to the {@link OutputStream}
     */
    public HttpClient send(final byte[] bytes) throws IOException {
        logger.finer(() -> String.format("Performing an initial connect on %s with output",
                url.toString()));
        getUrlConnection().setDoOutput(true);

        logger.finer(() -> "Sending binary data to remote system");
        try (OutputStream stream = getUrlConnection().getOutputStream()) {
            stream.write(bytes);
            stream.flush();
        }

        return this;
    }

    /**
     * <p>Provides access to the response code from the remote server. This
     * method when used must be called after one of the overloaded {@link #send()}
     * methods.</p>
     *
     * @return the HTTP response code from the remote server
     * @throws IOException if we fail to get a response from the remote server
     */
    public int responseCode() throws IOException {
        return ((HttpURLConnection) getUrlConnection()).getResponseCode();
    }

    /**
     * <p>The response body from the remote server as a {@link String}. Defaults
     * to {@link StandardCharsets#UTF_8}.</p>
     *
     * @return the body of the message as a {@link String}
     * @throws IOException if we fail to get a response from the remote server
     */
    public String body() throws IOException {
        if (responseCode() >= 200 && responseCode() < 300) {
            return StreamUtils.toString(getUrlConnection().getInputStream());
        } else {
            return StreamUtils.toString(((HttpURLConnection) getUrlConnection()).getErrorStream());
        }
    }

    /**
     * <p>Instructs that the response should be placed in an {@link OutputStream}
     * of the users choice.</p>
     *
     * @param outputStream the {@link OutputStream} that the content should be placed in
     * @throws IOException if {@link HttpURLConnection#getInputStream()} or
     *                     {@link HttpURLConnection#getErrorStream()} cannot be
     *                     read from
     */
    public void body(final OutputStream outputStream) throws IOException {
        if (responseCode() >= 200 && responseCode() < 300) {
            StreamUtils.inputToOutput(outputStream, getUrlConnection().getInputStream());
        } else {
            StreamUtils.inputToOutput(outputStream, ((HttpURLConnection) getUrlConnection()).getErrorStream());
        }
    }

    /**
     * <p>Returns the value of the named header field.</p>
     *
     * <p>If called on a connection that sets the same header multiple times
     * with possibly different values, only the last value is returned.</p>
     *
     * @param name the name of a header field.
     * @return the value of the named header field, or {@code null}
     * if there is no such field in the header.
     */
    public String responseHeader(final String name) {
        return getUrlConnection().getHeaderField(name);
    }

    /**
     * <p>Returns all header fields from the underlying HTTP connection.</p>
     *
     * @return all header fields from the underlying HTTP connection
     */
    public Map<String, List<String>> responseHeaders() {
        return getUrlConnection().getHeaderFields();
    }

    /**
     * <p>Configure HTTPS connection to trust all hosts. This method alleviates
     * the instances that the server using https does not use the name the
     * certificate provided.</p>
     *
     * <p>This method does nothing if the current request is not a HTTPS request.
     * This method should not be used in production.</p>
     *
     * @return this {@link HttpClient} object
     */
    public HttpClient trustAllHosts() {
        final HttpURLConnection urlConnection = (HttpURLConnection) getUrlConnection();
        if (urlConnection instanceof HttpsURLConnection) {
            ((HttpsURLConnection) urlConnection).setHostnameVerifier((s, sslSession) -> true);
        }
        return this;
    }

    /**
     * <p>Instructs {@link URLConnection} that it should disconnect. Whether it
     * actually disconnects or not is left to the JVM.</p>
     *
     * @return this {@link HttpClient} object
     */
    public HttpClient disconnect() {
        ((HttpURLConnection) getUrlConnection()).disconnect();
        return this;
    }

    @Override
    public void close() {
        disconnect();
    }

    /**
     * <p>Support for defining the HTTP Authorization header when basic
     * authentication is the authentication method. This method handles the
     * encoding of the username + password pair and adding the header to the
     * list of headers.</p>
     *
     * @param username the username to authenticate this request to
     * @param password the password associated to the user
     * @return this {@link HttpClient} object
     */
    public HttpClient setBasicAuth(final String username, final String password) {
        String value = "Basic " + encoder.encodeToString((username + ":" +
                password).getBytes());

        setRequestHeader(HttpHeader.AUTHORIZATION, value);
        return this;
    }

    /**
     * <p>Defines a HTTP header for a {@link URLConnection}. An HTTP header is
     * assigned immediately to a connection item.</p>
     *
     * @param key   the name of the HTTP header to be used
     * @param value the value that the HTTP header should have
     * @return this {@link HttpClient} object
     */
    public HttpClient setRequestHeader(final String key, final String value) {
        logger.finer(() -> String.format("Declaring a header key: %s value: %s",
                key, value));
        getUrlConnection().setRequestProperty(key, value);
        return this;
    }

    /**
     * <p>A type safe means of defining a HTTP header for a {@link URLConnection}.
     * An HTTP header is assigned immediately to a connection item.</p>
     *
     * @param header the name of the HTTP header to be used
     * @param value  the value that the HTTP header should have
     * @return this {@link HttpClient} object
     */
    public HttpClient setRequestHeader(final HttpHeader header, final String value) {
        logger.finer(() -> String.format("Declaring a header key: %s value: %s",
                header.toString(), value));
        getUrlConnection().setRequestProperty(header.toString(), value);
        return this;
    }

    /**
     * <p>Sets read timeout on connection to given value. The value provided
     * will be interpreted as seconds.</p>
     *
     * @param seconds the time period in seconds that the remote server must respond within
     * @return this {@link HttpClient} object
     */
    public HttpClient readTimeout(final int seconds) {
        logger.finer(() -> String.format("Defining read timeout: %s seconds", seconds));
        getUrlConnection().setReadTimeout((int) TimeUnit.SECONDS.toMillis(seconds));
        return this;
    }

    /**
     * <p>Set connect timeout on connection to given value.</p>
     *
     * @param seconds the time period in seconds that the remote server must
     *                accept our connection within
     * @return this {@link HttpClient} object
     */
    public HttpClient connectTimeout(final int seconds) {
        logger.finer(() -> String.format("Defining connect timeout: %s seconds", seconds));
        getUrlConnection().setConnectTimeout((int) TimeUnit.SECONDS.toMillis(seconds));
        return this;
    }

    public HttpClient setCharset(final Charset charset) {
        this.charset = charset;
        return this;
    }

    /**
     * <p></p>
     *
     * @return the current {@link URLConnection}
     */
    private URLConnection createConnection() {
        try {
            final URLConnection urlConnection = connectionFactory.create(url);
            ((HttpURLConnection) urlConnection).setRequestMethod(method.toString());
            return urlConnection;
        } catch (IOException e) {
            throw new HttpClientException(e);
        }
    }

    /**
     * <p>Gets the underlying connection with any configurations that may have
     * been performed.</p>
     *
     * @return the current {@link URLConnection}
     */
    public URLConnection getUrlConnection() {
        if (urlConnection == null) {
            urlConnection = createConnection();
        }

        return urlConnection;
    }

    /**
     * <p>Obtains the declared {@link URL} as a {@link String}</p>
     *
     * @return the current {@link URL} as a {@link String}
     */
    public String getUrl() {
        return url.toString();
    }

    /**
     * <p>The method that was defined on the HTTP call.</p>
     *
     * @return the {@link Method} that {@link URLConnection} will use
     */
    public Method getMethod() {
        return method;
    }

    /**
     * <p>Configures the {@link HttpsURLConnection} with the {@link SSLContextConfig}
     * interface.</p>
     *
     * @param sslContextConfig the configuration to be used when connecting to
     *                         HTTPS sites.
     * @return this {@link HttpClient} object
     */
    public HttpClient setSslContextConfig(final SSLContextConfig sslContextConfig) {
        SSLContextAccess sslContextAccess = new SSLContextAccess(sslContextConfig);
        try {
            ((HttpsURLConnection) getUrlConnection())
                    .setSSLSocketFactory(sslContextAccess.getSSLSocketFactory());
            return this;
        } catch (GeneralSecurityException e) {
            throw new HttpClientException(e);
        }
    }

    interface ConnectionFactory {

        /**
         * <p>A {@link ConnectionFactory} which uses the built-in
         * {@link URL#openConnection()}</p>
         */
        ConnectionFactory DEFAULT = new ConnectionFactory() {
            public URLConnection create(URL url) throws IOException {
                return url.openConnection();
            }

            public URLConnection create(URL url, Proxy proxy) throws IOException {
                return url.openConnection(proxy);
            }
        };

        /**
         * Open an {@link URLConnection} for the specified {@link URL}.
         *
         * @throws IOException
         */
        URLConnection create(URL url) throws IOException;

        /**
         * Open an {@link URLConnection} for the specified {@link URL}
         * and {@link Proxy}.
         *
         * @throws IOException
         */
        URLConnection create(URL url, Proxy proxy) throws IOException;
    }
}
