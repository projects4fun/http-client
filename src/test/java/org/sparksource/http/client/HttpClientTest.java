package org.sparksource.http.client;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileOutputStream;
import java.util.UUID;

/**
 * @author Juan Garcia
 * @since 2018-04-03
 */
class HttpClientTest {

    @Test
    void http_request_can_write_its_response_to_stream() {
        HttpClient httpClient = HttpClient.get("https://google.com");

        File file = new File("target/" + UUID.randomUUID().toString() + ".html");

        try (FileOutputStream fileOutputStream = new FileOutputStream(file)) {
            httpClient.body(fileOutputStream);
        } catch (Exception e) {
            Assertions.fail(e);
        }

        Assertions.assertTrue(file.exists());
    }

    @Test
    void http_request_can_return_its_own_url() {
        final String url = "https://reddit.com";

        HttpClient httpClient = HttpClient.get(url);

        Assertions.assertEquals(url, httpClient.getUrl());
    }

    @Test
    void http_request_that_writes_to_file_can_write_image() {
        String site = "https://www.redditstatic.com/desktop2x/img/favicon/apple-icon-57x57.png";
        HttpClient request = HttpClient.get(site);
        File image = new File("target/" + UUID.randomUUID().toString() + ".png");

        try (FileOutputStream fileOutputStream = new FileOutputStream(image)) {
            request.body(fileOutputStream);
        } catch (Exception e) {
            Assertions.fail(e);
        }

        Assertions.assertTrue(image.exists());
    }
}
