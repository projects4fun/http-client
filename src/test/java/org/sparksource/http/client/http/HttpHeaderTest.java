package org.sparksource.http.client.http;

import org.sparksource.http.client.HttpHeader;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * @author Juan Garcia
 * @since 2018-04-03
 */
class HttpHeaderTest {

    @Test
    void content_type_enum_provides_correct_text_value() {
        Assertions.assertEquals("Content-Type",
                HttpHeader.CONTENT_TYPE.toString());
    }
}
