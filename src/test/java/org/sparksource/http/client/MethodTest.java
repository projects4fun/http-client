package org.sparksource.http.client;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * @author Juan Garcia
 * @since 2018-04-03
 */
class MethodTest {

    @Test
    void post_method_to_string_does_return_post() {
        Assertions.assertEquals("POST", HttpClient.Method.POST.toString());
    }

    @Test
    void get_method_to_string_does_return_get() {
        Assertions.assertEquals("GET", HttpClient.Method.GET.toString());
    }

    @Test
    void head_method_to_string_does_return_head() {
        Assertions.assertEquals("HEAD", HttpClient.Method.HEAD.toString());
    }

    @Test
    void options_method_to_string_does_return_options() {
        Assertions.assertEquals("OPTIONS", HttpClient.Method.OPTIONS.toString());
    }

    @Test
    void put_method_to_string_does_return_put() {
        Assertions.assertEquals("PUT", HttpClient.Method.PUT.toString());
    }

    @Test
    void delete_method_to_string_does_return_delete() {
        Assertions.assertEquals("DELETE", HttpClient.Method.DELETE.toString());
    }

    @Test
    void trace_method_to_string_does_return_trace() {
        Assertions.assertEquals("TRACE", HttpClient.Method.TRACE.toString());
    }
}
