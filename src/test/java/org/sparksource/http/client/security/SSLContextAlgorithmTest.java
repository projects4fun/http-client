package org.sparksource.http.client.security;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * @author Juan Garcia
 * @since 2018-04-03
 */
class SSLContextAlgorithmTest {

    @Test
    void context_tls_does_return_the_string_tls() {
        Assertions.assertEquals("TLS", SSLContextAlgorithm.TLS.toString());
    }

    @Test
    void context_tls_v1_does_return_string_tls_v1() {
        Assertions.assertEquals("TLSv1", SSLContextAlgorithm.TLSv1.toString());
    }

    @Test
    void context_tls_v11_does_return_string_tls_v11() {
        Assertions.assertEquals("TLSv1.1", SSLContextAlgorithm.TLSv1_1.toString());
    }

    @Test
    void context_tls_v12_does_return_string_tls_v12() {
        Assertions.assertEquals("TLSv1.2", SSLContextAlgorithm.TLSv1_2.toString());
    }
}
