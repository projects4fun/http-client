package org.sparksource.http.client.security;

import org.sparksource.http.client.HttpClient;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;


/**
 * @author Juan Garcia
 * @since 2018-04-03
 */
class HttpClientTest {

    @Test
    void http_request_with_http_parameters_creates_valid_url() {
        // A series of HttpParameters are defined
        Map<String, String> parameters = new HashMap<>();
        parameters.put("id", "1");
        parameters.put("name", "John");

        // A HttpClient get is performed
        HttpClient httpClient = HttpClient.get("http://localhost:8080",
                parameters);

        Assertions.assertEquals("http://localhost:8080?name=John&id=1",
                httpClient.getUrl());
    }

    @Test
    void http_request_with_http_parameter_values_that_contains_space_creates_url() {
        // A series of HttpParameters are defined
        Map<String, String> parameters = new HashMap<>();
        parameters.put("id", "1");
        parameters.put("name", "John M Dexos");

        // A HttpClient get is performed
        HttpClient httpClient = HttpClient.get("http://localhost:8080",
                parameters);

        Assertions.assertEquals("http://localhost:8080?name=John%20M%20Dexos&id=1",
                httpClient.getUrl());
    }
}
