#### A URLConnection wrapper

##### API Overview

The API provides a series of static methods tied to `HttpClient` to define a 
new `URLConnection` instance. Any new `HttpClient` object must be created 
using the method type static methods.

    HttpClient client = HttpClient.get('https://google.com')

So long as we are calling those methods that modify the underlying `URLConnection` 
the object supports a fluent API one could for example do

    HttpClient client = HttpClient.post('http://localhost:8080/hello-word')
        .setRequestHeader("Content-Type", "application/xml")
        .setRequestHeader("Accept", "application/xml")
        .setBasicAuth("username", "password")
        .readTimeout(120)
        .send('DATA')

A call on one of the response methods however will return the type related to
the requested for. For example you would get the response code from the remote 
server by using

    int responseCode = client.responseCode()

#### Examples

##### Working with non String data

When retrieving an image you'll want to use the overloaded `body` method. The 
method takes a `OutputStream` as its argument. In this example we write the 
image out to a file.

    String site = "https://www.redditstatic.com/desktop2x/img/favicon/apple-icon-57x57.png";
    HttpClient client = HttpClient.get(site);
    File image = new File("target/" + UUID.randomUUID().toString() + ".png");

    try (FileOutputStream fileOutputStream = new FileOutputStream(image)) {
        client.body(fileOutputStream);
    } catch (Exception e) {
        e.printStackTrace();
    }

#### Dependencies

This library calls for no external dependencies but does require Java 8. Logging 
within the library is handled with JUL with most events tied to `FINER`. If you 
have a preferred logging library you will probably want to pull in its JUL
bridge.
